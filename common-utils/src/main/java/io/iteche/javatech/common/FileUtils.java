package io.iteche.javatech.common;


import org.springframework.util.FileCopyUtils;

import java.io.*;

/**
 * @author hegh1
 * @desc
 * @date 2023/2/1 9:43
 */
public class FileUtils {

    public static void main(String[] args) throws IOException {
        String content = "2023-01-29 14:16:08,869 INFO [main] org.apache.nifi.NiFi Launching NiFi...\n" +
                "2023-01-29 14:16:08,916 INFO [main] o.a.nifi.properties.NiFiPropertiesLoader Loading Application Properties [E:\\Projects\\apache-nifi\\nifi-assembly\\target\\NIFI-1~1.1\\.\\conf\\nifi.properties]\n" +
                "2023-01-29 14:16:08,916 WARN [main] o.a.nifi.properties.NiFiPropertiesLoader Generating Random Sensitive Properties Key [nifi.sensitive.props.key]\n" +
                "2023-01-29 14:16:09,201 INFO [main] o.a.nifi.properties.NiFiPropertiesLoader NiFi Properties [E:\\Projects\\apache-nifi\\nifi-assembly\\target\\NIFI-1~1.1\\.\\conf\\nifi.properties] updated with Sensitive Properties Key\n" +
                "2023-01-29 14:16:09,201 INFO [main] o.a.nifi.properties.NiFiPropertiesLoader Loading Application Properties [E:\\Projects\\apache-nifi\\nifi-assembly\\target\\NIFI-1~1.1\\.\\conf\\nifi.properties]\n" +
                "2023-01-29 14:16:09,201 INFO [main] org.apache.nifi.NiFi Application Properties loaded [202]\n" +
                "2023-01-29 14:16:09,214 INFO [main] org.apache.nifi.BootstrapListener Started Bootstrap Listener, Listening for incoming requests on port 56230\n" +
                "2023-01-29 14:16:09,229 INFO [main] org.apache.nifi.BootstrapListener Successfully initiated communication with Bootstrap\n" +
                "2023-01-29 14:16:09,246 INFO [main] org.apache.nifi.nar.NarUnpacker Expanding 124 NAR files with all processors...\n" +
                "2023-01-29 14:16:41,925 INFO [main] org.apache.nifi.nar.NarUnpacker NAR loading process took 32670974600 nanoseconds (32 seconds).\n" +
                "2023-01-29 14:16:45,435 INFO [main] org.apache.nifi.nar.NarClassLoaders Loaded NAR file: ";

        writeBytesToFile(content, "F:\\Output\\bin");
    }

    /**
     * 文件转为二进制数组
     * @param file
     * @return
     */
    public static byte[] fileToBinArray(File file){
        try {
            InputStream fis = new FileInputStream(file);
            byte[] bytes = FileCopyUtils.copyToByteArray(fis);
            return bytes;
        }catch (Exception ex){
            throw new RuntimeException("transform file into bin Array 出错",ex);
        }
    }

    /**
     * 文件转为二进制字符串
     * @param file
     * @return
     */
    public static String fileToBinStr(File file){
        try {
            InputStream fis = new FileInputStream(file);
            byte[] bytes = FileCopyUtils.copyToByteArray(fis);
            return new String(bytes,"UTF-8");
        }catch (Exception ex){
            throw new RuntimeException("transform file into bin String 出错",ex);
        }
    }


    /**
     * 二进制字符串转文件
     * @param bin
     * @param fileName
     * @param parentPath
     * @return
     */
    public static File binToFile(String bin,String fileName,String parentPath){
        try {
            File fout = new File(parentPath,fileName);
            fout.createNewFile();
            byte[] bytes1 = bin.getBytes("ISO-8859-1");
            FileCopyUtils.copy(bytes1,fout);

            //FileOutputStream outs = new FileOutputStream(fout);
            //outs.write(bytes1);
            //outs.flush();
            //outs.close();

            return fout;
        }catch (Exception ex){
            throw new RuntimeException("transform bin into File 出错",ex);
        }
    }

    /**
     * 文件转为二进制数组
     * 等价于fileToBin
     * @param file
     * @return
     */
    public static byte[] getFileToByte(File file) {
        byte[] by = new byte[(int) file.length()];
        try {
            InputStream is = new FileInputStream(file);
            ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
            byte[] bb = new byte[2048];
            int ch;
            ch = is.read(bb);
            while (ch != -1) {
                bytestream.write(bb, 0, ch);
                ch = is.read(bb);
            }
            by = bytestream.toByteArray();
        } catch (Exception ex) {
            throw new RuntimeException("transform file into bin Array 出错",ex);
        }
        return by;
    }

    public static void writeBytesToFile(String content, String dest) throws IOException {

        byte[] bs= content.getBytes();

        OutputStream out = new FileOutputStream(dest);

        InputStream is = new ByteArrayInputStream(bs);

        byte[] buff = new byte[1024];

        int len = 0;

        while((len=is.read(buff))!=-1){

            out.write(buff, 0, len);

        }

        is.close();

        out.close();

    }

    private static void readBinFile(String target) {
        try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(target)))) {
            String a, b;
            int c, d;
            a = dis.readUTF();
            c = dis.readInt();

            System.out.println(a);
            System.out.println(c);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeBinFile(String content, String dest)
    {
        try
        {
            //将DataOutputStream与FileOutputStream连接可输出不同类型的数据
            //FileOutputStream类的构造函数负责打开文件kuka.dat，如果文件不存在，
            //则创建一个新的文件，如果文件已存在则用新创建的文件代替。然后FileOutputStream
            //类的对象与一个DataOutputStream对象连接，DataOutputStream类具有写
            //各种数据类型的方法。
            DataOutputStream out=new DataOutputStream(new FileOutputStream(dest));
            out.write(content.getBytes());
            out.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

}
