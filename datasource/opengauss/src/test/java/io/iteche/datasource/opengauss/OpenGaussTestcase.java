package io.iteche.datasource.opengauss;

import java.sql.*;

/**
 * @author hegh1
 * @desc
 * @date 2023/2/1 17:56
 */
public class OpenGaussTestcase {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Connection connection = DriverManager.getConnection("jdbc:opengauss://42.194.135.29:9876/postgres", "opengauss", "Hegh@opengauss^_^618");
        String mergeSql = "merge into test target_table using (select ? as id, ? as name) as temporary_table on (target_table.id=temporary_table.id)\n" +
                "when matched then update set target_table.name=temporary_table.name\n" +
                "when not matched then insert values(temporary_table.id, temporary_table.name)";
        PreparedStatement preparedStatement = connection.prepareStatement(mergeSql);
        preparedStatement.setObject(1, 2);
        preparedStatement.setObject(2, "asd2123as");
        preparedStatement.addBatch();
        preparedStatement.setObject(1, 3);
        preparedStatement.setObject(2, "asaasdasd");
        preparedStatement.addBatch();
        preparedStatement.executeBatch();
    }

}
