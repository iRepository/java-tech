1. 创建项目路径，用来创建项目

   ![image-20230605190801659](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605190801659.png)

2. idea新建项目
   ![image-20230605190959681](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605190959681.png)

   ![image-20230605191102636](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605191102636.png)

   ![image-20230605191655173](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605191655173.png)

   ![image-20230605191753866](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605191753866.png)

3. 配置idea的maven及其他

   ![image-20230605191852114](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605191852114.png)

   ![image-20230605191951819](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605191951819.png)

   ![image-20230605192049218](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605192049218.png)

   ![image-20230605192249922](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605192249922.png)

   ![image-20230605192429216](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605192429216.png)

4. 引入springboot的父项目，使你的项目变成一个springboot项目

   ```xml
   <?xml version="1.0" encoding="UTF-8"?>
   <project xmlns="http://maven.apache.org/POM/4.0.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
       <modelVersion>4.0.0</modelVersion>
   
       <parent>
           <groupId>org.springframework.boot</groupId>
           <artifactId>spring-boot-starter-parent</artifactId>
           <version>2.3.12.RELEASE</version>
       </parent>
   
       <groupId>io.javatech</groupId>
       <artifactId>springboot-template</artifactId>
       <version>1.0-SNAPSHOT</version>
   
       <properties>
           <maven.compiler.source>8</maven.compiler.source>
           <maven.compiler.target>8</maven.compiler.target>
       </properties>
   
       <dependencies>
           <dependency>
               <groupId>org.springframework.boot</groupId>
               <artifactId>spring-boot-starter-web</artifactId>
           </dependency>
       </dependencies>
   
       <build>
           <plugins>
               <plugin>
                   <groupId>org.springframework.boot</groupId>
                   <artifactId>spring-boot-maven-plugin</artifactId>
               </plugin>
           </plugins>
       </build>
   
   </project>
   ```

   ![image-20230605193244571](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193244571.png)

5. 建基础包和主应用类

   ![image-20230605193335680](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193335680.png)

   ![image-20230605193412975](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193412975.png)

   ![image-20230605193447044](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193447044.png)

6. 使用注解声明springboot应用主类

   ![image-20230605193555653](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193555653.png)

   ```java
   package io.javatech;
   
   import org.springframework.boot.SpringApplication;
   import org.springframework.boot.autoconfigure.SpringBootApplication;
   
   /**
    * @author hegh1
    * @desc
    * @date 2023/6/5 19:34
    */
   @SpringBootApplication
   public class SpringBootTemplateApplication {
   
       public static void main(String[] args) {
           SpringApplication.run(SpringBootTemplateApplication.class, args);
       }
   
   }
   
   ```

7. 编写一个测试controller

   ![image-20230605193646229](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193646229.png)

   ![image-20230605193732766](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193732766.png)

   ![image-20230605193845384](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193845384.png)

   ```java
   package io.javatech.controller;
   
   import org.springframework.web.bind.annotation.GetMapping;
   import org.springframework.web.bind.annotation.RestController;
   
   /**
    * @author hegh1
    * @desc
    * @date 2023/6/5 19:37
    */
   @RestController
   public class TestController {
   
       @GetMapping("/test")
       String test() {
          return "hello, spring boot!";
       }
   
   }
   ```

8. idea启动程序

   ![image-20230605193959165](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605193959165.png)

   ![image-20230605194138353](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605194138353.png)

9. 发送请求测试接口

   ![image-20230605194224460](C:\Users\hegh1\AppData\Roaming\Typora\typora-user-images\image-20230605194224460.png)

   浏览器发送[localhost:8080/test](http://localhost:8080/test)