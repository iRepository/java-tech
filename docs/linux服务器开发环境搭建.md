> linux服务器准备

1. 创建一个新的非root用户

   ```shell
   useradd hegh -d /home/hegh
   passwd hegh
   ```

2. 切换该用户进行环境搭建操作

   ```shell
   su hegh
   # 创建该路径用于环境配置的根路径
   sudo mkdir -p /envcfg
   # 创建该路径用于存放环境安装包的根路径
   sudo mkdir -p /packages
   # 创建仓库根路径
   sudo mkdir -p /repositories
   sudo mkdir -p /repositories/maven
   sudo mkdir -p /repositories/gradle
   # 创建springboot jar包根路径
   sudo mkdir -p /bootjars
   # 创建项目根路径
   sudo mkdir -p /projects
   # 后端
   sudo mkdir -p /projects/backend
   # 前端
   sudo mkdir -p /projects/front
   ```

- jdk环境配置

  [jdk官网历史版本下载列表](https://www.oracle.com/java/technologies/downloads/)

  [华为jdk镜像下载](https://repo.huaweicloud.com/java/jdk/)

  **oracle官网下载速度很慢，推荐使用国内镜像源下载**

  ```shell
  sudo mkdir -p /envcfg/jdk
  cd /packages
  # sudo wget https://repo.huaweicloud.com/java/jdk/8u181-b13/jdk-8u181-linux-x64.tar.gz
  # 将下载好的jdk安装包上传到/packages路径下
  # 安装jdk到/envcfg/jdk下
  sudo tar -zxvf jdk-8u341-linux-x64.tar.gz -C /envcfg/jdk
  cd /envcfg/jdk/jdk1.8.0_341
  pwd
  cd /etc/profile.d/
  sudo vim jdk.sh
  # 配置jdk环境变量
  export JAVA_HOME=/envcfg/jdk/jdk1.8.0_341
  export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
  export PATH=$PATH:$JAVA_HOME/bin
  # 使配置文件生效
  source /etc/profile
  # 验证
  java -version
  ```

- maven环境配置

  [maven官网历史版本下载列表](https://archive.apache.org/dist/maven/maven-3/)

  ```shell
  sudo mkdir -p /envcfg/maven
  cd /packages
  # 选择相应版本下载
  sudo wget https://archive.apache.org/dist/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz
  # 安装到/envcfg/maven下
  sudo tar -zxvf apache-maven-3.8.1-bin.tar.gz -C /envcfg/maven
  cd /envcfg/maven/apache-maven-3.8.1
  pwd
  cd /etc/profile.d/
  sudo vim maven.sh
  # 配置maven环境变量
  MAVEN_HOME=/envcfg/maven/apache-maven-3.8.1
  export PATH=$PATH:$MAVEN_HOME/bin
  # 使配置文件生效
  source /etc/profile
  # 验证
  mvn -v
  
  # maven配置阿里云镜像仓库
  cd /envcfg/maven/apache-maven-3.8.1/conf
  sudo cp settings.xml settings.xml.bak
  # 编辑settings.xml
  sudo vim settings.xml
  
  # 配置maven本地仓库
  ------------------------------
  <localRepository>/repositories/maven</localRepository>
  ------------------------------
  # 配置阿里云镜像
  ------------------------------
  <mirror>
      <id>aliyunmaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>
  </mirror>
  ------------------------------
  # 配置使用jdk8编译
  ------------------------------
  <profile>
      <id>jdk1.8</id>
      <activation>
          <activeByDefault>true</activeByDefault>
          <jdk>1.8</jdk>
      </activation>
      <properties>
          <maven.compiler.source>1.8</maven.compiler.source>
          <maven.compiler.target>1.8</maven.compiler.target>
          <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
      </properties>
  </profile>
  ------------------------------
  ```

- gradle环境配置

  [gradle官网历史版本下载列表](https://gradle.org/releases/)

  ```shell
  sudo mkdir -p /envcfg/gradle
  sudo vim /repositories/gradle/init.gradle
  # 输入一下内容配置阿里云镜像
  --------------------
  allprojects{
      repositories {
          def REPOSITORY_URL = 'https://maven.aliyun.com/nexus/content/groups/public/'
          all { ArtifactRepository repo ->
              if(repo instanceof MavenArtifactRepository){
                  def url = repo.url.toString()
                  if (url.startsWith('https://repo1.maven.org/maven2') || url.startsWith('https://jcenter.bintray.com/')) {
                      project.logger.lifecycle "Repository ${repo.url} replaced by $REPOSITORY_URL."
                      remove repo
                  }
              }
          }
          maven {
              url REPOSITORY_URL
          }
      }
  }
  --------------------
  cd /packages
  # 将下载好的gradle压缩包上传到/packages路径下
  # 解压到/envcfg/gradle下
  sudo unzip -d /envcfg/gradle gradle-7.5.1-bin.zip
  cd /envcfg/gradle/gradle-7.5.1
  pwd
  sudo vim /etc/profile
  # 配置环境变量
  GRADLE_HOME=/envcfg/gradle/gradle-7.5.1
  export PATH=$PATH:$GRADLE_HOME/bin
  # 配置gradle本地仓库地址
  GRADLE_USER_HOME=/repositories/gradle
  # 使配置文件生效
  source /etc/profile
  # 验证
  gradle --version
  -----------------------------------------------------------------------------------------------------------------------------
  # gradle编译推送docker 官网教程[https://bmuschko.github.io/gradle-docker-plugin/]
  
  ```

- git环境配置

  ```shell
  sudo yum -y install git
  # 验证
  git --version
  # 绑定邮箱
  ssh-keygen -t rsa -C "hgh_wy163mail@163.com"
  # 连续敲三次回车键
  cat ~/.ssh/id_rsa.pub
  # 复制密钥配置到gitee的ssh公钥
  # 验证配置后是否生效
  ssh -T git@gitee.com
  # 输入yes，成功输出
  Warning: Permanently added 'gitee.com,212.64.63.215' (ECDSA) to the list of known hosts.
  Hi xxx! You've successfully authenticated, but GITEE.COM does not provide shell access.
  ```

- npm

  ```shell
  sudo mkdir -p /envcfg/npm
  sudo dnf install -y npm
  npm config set registry https://registry.npm.taobao.org
  ```

- docsify

  ```shell
  sudo npm i docsify-cli -g
  # 如果-bash: docsify: command not found
  sudo find / -name docsify
  # 创建软连接
  ln -s /envcfg/npm/node_global/bin/docsify /usr/local/bin
  # 验证
  docsify -v
  ```

  