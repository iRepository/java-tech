- docker安装opengauss

[docker安装openGauss数据库_opengauss docker_万山寒的博客-CSDN博客](https://blog.csdn.net/blood_Z/article/details/128001464)

```shell
docker pull enmotech/opengauss
docker run --name opengauss --privileged=true -d -e GS_USERNAME='opengauss' -e GS_PASSWORD='Hegh@opengauss^_^618' -e GS_PORT=15432 -p 15432:5432 enmotech/opengauss
#数据卷挂载
docker run --name opengauss --privileged=true -d -e GS_NODENAME='gaussdb' -e GS_USERNAME='opengauss' -e GS_PASSWORD='Hegh@opengauss^_^618' -e GS_PORT=9876 -p 9876:9876 -v /datavolumes/opengauss/data:/var/lib/opengauss/data enmotech/opengauss
```
```shell
#命令行登录gaussdb
docker exec -it opengauss /bin/bash
su - omm
gsql -U opengauss -W Hegh@opengauss^_^618 -d postgres -p 9876
```
```mysql
#创建序列
CREATE SEQUENCE sample_table_id_seq cache 100;
#创建自增主键的表
create table sample_table(
    id int not null default nextval('sample_table_id_seq'),
    name varchar(100) default '',
    amount decimal(20,2) default null,
    create_date date default null,
    create_time timestamp null,
    content text default null,
    constraint pk_sample_table_id primary key(id)
);
comment on table sample_table is '示例表';
comment on column sample_table.name is 'name';
comment on column sample_table.amount is 'amount';
comment on column sample_table.create_date is 'create_date';
comment on column sample_table.create_time is 'create_time';
comment on column sample_table.content is 'content';
```
创建容器的参数解析
privileged 使docker容器获取真正的root权限

GS_PASSWORD

使用openGauss镜像的时候，必须设置该参数。该参数值不能为空或者不定义。该参数设置了openGauss数据库的超级用户omm以及测试用户gaussdb的密码。openGauss安装时默认会创建omm超级用户，该用户名暂时无法修改。测试用户gaussdb是在entrypoint.sh中自定义创建的用户。

openGauss镜像配置了本地信任机制，因此在容器内连接数据库无需密码，但是如果要从容器外部（其它主机或者其它容器）连接则必须要输入密码。

openGauss的密码有复杂度要求

密码长度8个字符以上，必须同时包含大写字母、小写字母、数字、以及特殊符号（特殊符号仅包含“#?!@%^&*-”，并且“!&”需要用转义符“\”进行转义）。

GS_NODENAME

指定数据库节点名称，默认为gaussdb。

GS_USERNAME

指定数据库连接用户名，默认为gaussdb。

GS_PORT
