[PostgreSQL: Documentation](https://www.postgresql.org/docs/)

[PostgreSQL: Documentation: Data Types](https://www.postgresql.org/docs/current/datatype.html)

- 创建自增id主键的表

  ```mysql
  CREATE SEQUENCE sample_table_id_seq start 1;
  create table sample_table(
      id int not null default nextval('sample_table_id_seq'::regclass),
      name varchar(100) default '',
      amount decimal(20,2) default null,
      create_date date default null,
      create_time timestamp null,
      content text default null,
      constraint pk_sample_table_id primary key(id)
  );
  comment on table sample_table is '示例表';
  comment on column sample_table.name is 'name';
  comment on column sample_table.amount is 'amount';
  comment on column sample_table.create_date is 'create_date';
  comment on column sample_table.create_time is 'create_time';
  comment on column sample_table.content is 'content';
  ```

- 创建数据库

  ```mysql
  create database nifi;
  ```

  ```mysql
  createdb mydb
  /usr/local/pgsql/bin/createdb mydb
  ```

- 删除数据库

  ```mysql
  drop table testing_sqlserver_table;
  ```

  ```mysql
  dropdb mydb
  ```

- 创建表

  ```mysql
  CREATE TABLE multitype_column_table (
      id int not null,
      date date default null
  );
  ```

- 删除表

  ```mysql
  DROP TABLE tablename;
  ```

- 插入数据到表

  ```mysql
  insert into multitype_column_table select 1,current_date;
  -- 分隔符按实际文件中的分隔符
  copy multitype_column_table from 'D:\learning-notes\database\postgresql\load_data.txt' (DELIMITER '|');
  ```

- 查询表数据

  ```mysql
  select * from multitype_column_table;
  ```

- 联表查询

  ```mysql
  select * from a join b on a.xx=b.xx;
  
  select c1,c2 from a join b where b.xx=a.c1;
  
  select * from a left outer join b on a.xx = b.xx;
  ```

- 聚合函数

  ```mysql
  max(column)
  count(*)/count(column)/count(1)
  ```

- 更新

- 删除

- 外键

  ```mysql
  CREATE TABLE cities (
          name     varchar(80) primary key,
          location point
  );
  
  CREATE TABLE weather (
          city      varchar(80) references cities(name),
          temp_lo   int,
          temp_hi   int,
          prcp      real,
          date      date
  );
  ```

- 事务

  ```mysql
  BEGIN;
  UPDATE accounts SET balance = balance - 100.00
      WHERE name = 'Alice';
  -- etc etc
  COMMIT;
  ```

  ```mysql
  BEGIN;
  UPDATE accounts SET balance = balance - 100.00
      WHERE name = 'Alice';
  SAVEPOINT my_savepoint;
  UPDATE accounts SET balance = balance + 100.00
      WHERE name = 'Bob';
  -- oops ... forget that and use Wally's account
  ROLLBACK TO my_savepoint;
  UPDATE accounts SET balance = balance + 100.00
      WHERE name = 'Wally';
  COMMIT;
  ```

- 继承

  ```mysql
  create table parent_table(
  	id int not null,
  	name varchar(50)
  );
  
  create table child_table(
  	age int  not null
  ) INHERITS (parent_table);
  ```

- 函数定义

  ```mysql
  CREATE FUNCTION dept(text) RETURNS dept
      AS $$ SELECT * FROM dept WHERE name = $1 $$
      LANGUAGE SQL;
  example:
  CREATE FUNCTION queryByName(text) RETURNS int AS $$ SELECT id FROM parent_table WHERE name = $1 $$ LANGUAGE SQL;
  --执行
  select queryByName('gasd');
  ```

- 校验约束

  ```mysql
  CREATE TABLE products (
      product_no integer,
      name text,
      price numeric CHECK (price > 0)
  );
  
  CREATE TABLE products (
      product_no integer,
      name text,
      price numeric CONSTRAINT positive_price CHECK (price > 0)
  );
  
  CREATE TABLE products (
      product_no integer,
      name text,
      price numeric CHECK (price > 0),
      discounted_price numeric CHECK (discounted_price > 0),
      CHECK (price > discounted_price)
  );
  ```

- 新增列

  ```mysql
  ALTER TABLE products ADD COLUMN description text;
  ```

- 移除列

  ```mysql
  ALTER TABLE products DROP COLUMN description;
  --增加授权
  ALTER TABLE products DROP COLUMN description CASCADE;
  ```

- 添加约束

  ```mysql
  ALTER TABLE products ADD CHECK (name <> '');
  ALTER TABLE products ADD CONSTRAINT some_name UNIQUE (product_no);
  ALTER TABLE products ADD FOREIGN KEY (product_group_id) REFERENCES product_groups;
  ALTER TABLE products ALTER COLUMN product_no SET NOT NULL;
  ```

- 移除约束

  ```mysql
  ALTER TABLE products DROP CONSTRAINT some_name;
  ALTER TABLE products ALTER COLUMN product_no DROP NOT NULL;
  ```

- 改变列的默认值

  ```
  ALTER TABLE products ALTER COLUMN price SET DEFAULT 7.77;
  --移除列的默认值
  ALTER TABLE products ALTER COLUMN price DROP DEFAULT;
  ```

- 改变列的类型

  ```mysql
  ALTER TABLE products ALTER COLUMN price TYPE numeric(10,2);
  ```

- 改变列的类型

  ```mysql
  ALTER TABLE products ALTER COLUMN price TYPE numeric(10,2);
  ```

- 重命名列

  ```mysql
  ALTER TABLE products RENAME COLUMN product_no TO product_number;
  ```

- 重命名表

  ```mysql
  ALTER TABLE products RENAME TO items;
  ```

- 修改表权限

  ```mysql
  ALTER TABLE table_name OWNER TO new_owner;
  ```

- 给用户授权

  ```mysql
  GRANT UPDATE ON accounts TO joe;
  ```

- 撤销授权

  ```mysql
  REVOKE ALL ON accounts FROM PUBLIC;
  ```

- 创建schema

  ```
  CREATE SCHEMA myschema;
  schema.table
  database.schema.table
  CREATE TABLE myschema.mytable (
   ...
  );
  DROP SCHEMA myschema;
  DROP SCHEMA myschema CASCADE;
  ```

- schema搜索路径

  ```mysql
  SHOW search_path;
   search_path
  --------------
   "$user", public
   
  SET search_path TO myschema,public;
  DROP TABLE mytable;
  SET search_path TO myschema;
  ```

- 撤销schema权限

  ```mysql
  REVOKE CREATE ON SCHEMA public FROM PUBLIC;
  ```

- 表分区

  - Range Partitioning

    ```mysql
    CREATE TABLE measurement (
        city_id         int not null,
        logdate         date not null,
        peaktemp        int,
        unitsales       int
    ) PARTITION BY RANGE (logdate);
    
    CREATE TABLE measurement_y2006m02 PARTITION OF measurement
        FOR VALUES FROM ('2006-02-01') TO ('2006-03-01');
    
    CREATE TABLE measurement_y2006m03 PARTITION OF measurement
        FOR VALUES FROM ('2006-03-01') TO ('2006-04-01');
    
    ...
    CREATE TABLE measurement_y2007m11 PARTITION OF measurement
        FOR VALUES FROM ('2007-11-01') TO ('2007-12-01');
    
    CREATE TABLE measurement_y2007m12 PARTITION OF measurement
        FOR VALUES FROM ('2007-12-01') TO ('2008-01-01')
        TABLESPACE fasttablespace;
    
    CREATE TABLE measurement_y2008m01 PARTITION OF measurement
        FOR VALUES FROM ('2008-01-01') TO ('2008-02-01')
        WITH (parallel_workers = 4)
        TABLESPACE fasttablespace;
        
    CREATE TABLE measurement_y2006m02 PARTITION OF measurement
        FOR VALUES FROM ('2006-02-01') TO ('2006-03-01')
        PARTITION BY RANGE (peaktemp);
        
    CREATE INDEX ON measurement (logdate);
    
    --Ensure that the enable_partition_pruning configuration parameter is not disabled in postgresql.conf. If it is, queries will not be optimized as desired.
    ```

  - List Partitioning

  - Hash Partitioning

- 分区维护

  ```mysql
  DROP TABLE measurement_y2006m02;
  ALTER TABLE measurement DETACH PARTITION measurement_y2006m02;
  ALTER TABLE measurement DETACH PARTITION measurement_y2006m02 CONCURRENTLY;
  ```

- 分区继承

  ```mysql
  CREATE TABLE measurement (
      city_id         int not null,
      logdate         date not null,
      peaktemp        int,
      unitsales       int
  );
  
  CREATE TABLE measurement_y2006m02 () INHERITS (measurement);
  CREATE TABLE measurement_y2006m03 () INHERITS (measurement);
  ...
  CREATE TABLE measurement_y2007m11 () INHERITS (measurement);
  CREATE TABLE measurement_y2007m12 () INHERITS (measurement);
  CREATE TABLE measurement_y2008m01 () INHERITS (measurement);
  
  CREATE OR REPLACE FUNCTION measurement_insert_trigger()
  RETURNS TRIGGER AS $$
  BEGIN
      INSERT INTO measurement_y2008m01 VALUES (NEW.*);
      RETURN NULL;
  END;
  $$
  LANGUAGE plpgsql;
  
  
  CREATE TRIGGER insert_measurement_trigger
      BEFORE INSERT ON measurement
      FOR EACH ROW EXECUTE FUNCTION measurement_insert_trigger();
  ```

- 插入数据并返回行id

  ```mysql
  INSERT INTO users (firstname, lastname) VALUES ('Joe', 'Cool') RETURNING id;
  ```

- 自增id

  ```mysql
  CREATE TABLE tablename (
      colname SERIAL
  );
  
  CREATE SEQUENCE tablename_colname_seq AS integer;
  CREATE TABLE tablename (
      colname integer NOT NULL DEFAULT nextval('tablename_colname_seq')
  );
  ALTER SEQUENCE tablename_colname_seq OWNED BY tablename.colname;
  
  example:
  CREATE SEQUENCE autoincrement_table_id_seq AS bigint;
  CREATE TABLE autoincrement_table (
      id SERIAL NOT NULL,
      name varchar(50) default '',
      constraint pk_autoincrement_table_id primary key(id)
  );
  
  insert into autoincrement_table(name)values(gen_random_uuid()) returning id;
  ```

  