[sqlserver官网使用文档](https://learn.microsoft.com/zh-cn/sql/relational-databases/databases/create-a-database?view=sql-server-ver15)

> 创建自增主键id的表

```mysql
create table sample_table(
    id bigint identity(1,1) not null,
    name varchar(100) default '',
    amount decimal(20,2) default null,
    create_date date default null,
    create_time timestamp null,
    content text default null,
    primary key (id)
);
EXEC sp_addextendedproperty N'MS_Description', N'示例表', N'user', N'dbo', N'table', N'sample_table', NULL, NULL;
EXEC sp_addextendedproperty N'MS_Description', N'id', N'user', N'dbo', N'table', N'sample_table', N'column', N'id';
EXEC sp_addextendedproperty N'MS_Description', N'name', N'user', N'dbo', N'table', N'sample_table', N'column', N'name';
EXEC sp_addextendedproperty N'MS_Description', N'amount', N'user', N'dbo', N'table', N'sample_table', N'column', N'amount';
EXEC sp_addextendedproperty N'MS_Description', N'create_date', N'user', N'dbo', N'table', N'sample_table', N'column', N'create_date';
EXEC sp_addextendedproperty N'MS_Description', N'create_time', N'user', N'dbo', N'table', N'sample_table', N'column', N'create_time';
EXEC sp_addextendedproperty N'MS_Description', N'content', N'user', N'dbo', N'table', N'sample_table', N'column', N'content';
```

> 数据类型

```
一、 整数数据类型
 整数数据类型是最常用的数据类型之一。
 1、INT （INTEGER）
     INT （或INTEGER）数据类型存储从-2的31次方 （-2 ，147 ，483 ，648） 到2的31次方-1 （2 ，147 ，483，647） 之间的所有正负整数。
     每个INT 类型的数据按4 个字节存储，其中1 位表示整数值的正负号，其它31 位表示整数值的长度和大小。
 2、SMALLINT
     SMALLINT 数据类型存储从-2的15次方（ -32， 768） 到2的15次方-1（ 32 ，767 ）之间的所有正负整数。
     每个SMALLINT 类型的数据占用2 个字节的存储空间，其中1 位表示整数值的正负号，其它15 位表示整数值的长度和大小。
 3、TINYINT
     TINYINT数据类型存储从0 到255 之间的所有正整数。每个TINYINT类型的数据占用1 个字节的存储空间。
 4、BIGINT
     BIGINT 数据类型存储从-2^63 （-9 ，223， 372， 036， 854， 775， 807） 到2^63-1（ 9， 223， 372， 036 ，854 ，775， 807） 之间的所有正负整数。
     每个BIGINT 类型的数据占用8个字节的存储空间。

二、 浮点数据类型
 浮点数据类型用于存储十进制小数。浮点数值的数据在SQL Server 中采用上舍入（Round up 或称为只入不舍）方式进行存储。
 所谓上舍入是指，当（且仅当）要舍入的数是一个非零数时，对其保留数字部分的最低有效位上的数值加1 ，并进行必要的进位。若一个数是上舍入数，其绝对值不会减少。
 如：对3.14159265358979 分别进行2 位和12位舍入，结果为3.15 和3.141592653590。
 1、REAL 数据类型
     REAL数据类型可精确到第7 位小数，其范围为从-3.40E -38 到3.40E +38。 每个REAL类型的数据占用4 个字节的存储空间。
 2、FLOAT
     FLOAT 数据类型可精确到第15 位小数，其范围为从-1.79E -308 到1.79E +308。 每个FLOAT 类型的数据占用8 个字节的存储空间。
     FLOAT数据类型可写为FLOAT[ n ]的形式。n 指定FLOAT 数据的精度。n 为1到15 之间的整数值。当n 取1 到7 时，实际上是定义了一个REAL 类型的数据，系统用4 个字节存储它；
     当n 取8 到15 时，系统认为其是FLOAT 类型，用8 个字节存储它。
 3、DECIMAL
     DECIMAL数据类型可以提供小数所需要的实际存储空间，但也有一定的限制，您可以用2 到17 个字节来存储从-10的38次方-1 到10的38次方-1 之间的数值。
     可将其写为DECIMAL[ p [s] ]的形式，p 和s 确定了精确的比例和数位。其中p 表示可供存储的值的总位数（不包括小数点），缺省值为18； s 表示小数点后的位数，缺省值为0。
     例如：decimal （15 5），表示共有15 位数，其中整数10 位，小数5。  
 4、NUMERIC
     NUMERIC数据类型与DECIMAL数据类型完全相同。
 注意：SQL Server 为了和前端的开发工具配合，其所支持的数据精度默认最大为28位。但可以通过使用命令来执行sqlserver.exe程序以启动SQL Server，可改变默认精度。
 命令语法如下：
     SQLSERVR[/D master_device_path][/P precisim_leve1]
 例4-4: 用最大数据精度38 启动SQL Server
     sqlservr /d c:\ Mssql2000\data\master.dat /p38
     /*在使用了/P 参数后,如果其后没有指定具体的精度数值,则默认为38 位./*

三、二进制数据类型
 1、BINARY
     BINARY 数据类型用于存储二进制数据。其定义形式为BINARY（ n）， n 表示数据的长度，取值为1 到8000 。
     在使用时必须指定BINARY 类型数据的大小，至少应为1 个字节。BINARY 类型数据占用n+4 个字节的存储空间。
     在输入数据时必须在数据前加上字符“0X” 作为二进制标识，如：要输入“abc ”则应输入“0xabc ”。
     若输入的数据过长将会截掉其超出部分。若输入的数据位数为奇数，则会在起始符号“0X ”后添加一个0，如上述的“0xabc ”会被系统自动变为“0x0abc”。
 2、VARBINARY
     VARBINARY 数据类型的定义形式为VARBINARY（n）。 它与BINARY 类型相似，n 的取值也为1 到8000，若输入的数据过长，将会截掉其超出部分。
     不同的是VARBINARY数据类型具有变动长度的特性，因为VARBINARY数据类型的存储长度为实际数值长度+4个字节。当BINARY数据类型允许NULL 值时，将被视为VARBINARY数据类型。
     一般情况下，由于BINARY 数据类型长度固定，因此它比VARBINARY 类型的处理速度快。

四、 逻辑数据类型
     BIT： BIT数据类型占用1 个字节的存储空间，其值为0 或1 。如果输入0 或1 以外的值，将被视为1。
     BIT 类型不能定义为NULL 值（所谓NULL 值是指空值或无意义的值）。

五、字符数据类型
 字符数据类型是使用最多的数据类型。它可以用来存储各种字母、数字符号、特殊符号。一般情况下，使用字符类型数据时须在其前后加上单引号’或双引号” 。
 1 CHAR
     CHAR 数据类型的定义形式为CHAR[ （n） 。 以CHAR 类型存储的每个字符和符号占一个字节的存储空间。n 表示所有字符所占的存储空间，n 的取值为1 到8000，
     即可容纳8000 个ANSI 字符。若不指定n 值，则系统默认值为1。若输入数据的字符数小于n，则系统自动在其后添加空格来填满设定好的空间。若输入的数据过长，将会截掉其超出部分。
 2、NCHAR
     NCHAR 数据类型的定义形式为NCHAR[ （n）]。 它与CHAR 类型相似。不同的是NCHAR数据类型n 的取值为1 到4000。 因为NCHAR 类型采用UNICODE 标准字符集（CharacterSet）。
     UNICODE 标准规定每个字符占用两个字节的存储空间，所以它比非UNICODE 标准的数据类型多占用一倍的存储空间。使用UNICODE 标准的好处是因其使用两个字节做存储单位，
     其一个存储单位的容纳量就大大增加了，可以将全世界的语言文字都囊括在内，在一个数据列中就可以同时出现中文、英文、法文、德文等，而不会出现编码冲突。
 3、VARCHAR
     VARCHAR数据类型的定义形式为VARCHARn）。 它与CHAR 类型相似，n 的取值也为1 到8000，若输入的数据过长，将会截掉其超出部分。
     不同的是，VARCHAR数据类型具有变动长度的特性，因为VARCHAR数据类型的存储长度为实际数值长度，若输入数据的字符数小于n ，则系统不会在其后添加空格来填满设定好的空间。
     一般情况下，由于CHAR 数据类型长度固定，因此它比VARCHAR 类型的处理速度快。
 4、NVARCHAR
     NVARCHAR数据类型的定义形式为NVARCHAR[ （n） ]。 它与VARCHAR 类型相似。不同的是，NVARCHAR数据类型采用UNICODE 标准字符集（Character Set）， n 的取值为1 到4000。

六、文本和图形数据类型
 这类数据类型用于存储大量的字符或二进制数据。
 1、TEXT
     TEXT数据类型用于存储大量文本数据，其容量理论上为1 到2的31次方-1 （2， 147， 483， 647）个字节，在实际应用时需要视硬盘的存储空间而定。
     SQL Server 2000 以前的版本中，数据库中一个TEXT 对象存储的实际上是一个指针，它指向一个个以8KB （8192 个字节）为单位的数据页（Data Page）。
     这些数据页是动态增加并被逻辑链接起来的。在SQL Server 2000 中，则将TEXT 和IMAGE 类型的数据直接存放到表的数据行中，而不是存放到不同的数据页中。
     这就减少了用于存储TEXT 和IMA- GE 类型的空间，并相应减少了磁盘处理这类数据的I/O 数量。
 2 NTEXT
     NTEXT数据类型与TEXT.类型相似不同的,是NTEXT 类型采用UNICODE 标准字符集(Character Set), 因此其理论容量为230-1(1, 073, 741, 823)个字节。
 3 IMAGE
     IMAGE 数据类型用于存储大量的二进制数据Binary Data。其理论容量为2的31次方-1(2,147,483,647)个字节。其存储数据的模式与TEXT 数据类型相同。
     通常用来存储图形等OLE Object Linking and Embedding，对象连接和嵌入）对象。在输入数据时同BINARY数据类型一样，必须在数据前加上字符“0X”作为二进制标识

七、 日期和时间数据类型
 1 DATETIME
     DATETIME 数据类型用于存储日期和时间的结合体。它可以存储从公元1753 年1 月1 日零时起到公元9999 年12 月31 日23 时59 分59 秒之间
```

- 创建自增id主键的表

  ```
  id bigint identity(1,1) not null PRIMARY key,
  ```

  

- 查询有哪些数据库

  ```mysql
  select name from master..sysdatabases;
  ```

- 查询有哪些表

  ```mysql
  select name from master..sysobjects where xtype='U';
  -- XType='U':表示所有用户表; 
  -- XType='S':表示所有系统表; 
  ```

- 查询某个表有哪些列

  ```mysql
  select name from master..syscolumns where id=object_id('table_name');
  ```

- 创建数据库

  ```mysql
  USE master;  
  GO  
  CREATE DATABASE nifi
  ON
  ( NAME = nifi_dat,
   	-- 自己sqlserver的DATA路径
      FILENAME = 'D:\databases\sqlserver\instance\MSSQL16.MSSQLSERVER\MSSQL\DATA\nifidat.mdf',
      SIZE = 10,
      MAXSIZE = 50,
      FILEGROWTH = 5 )
  LOG ON
  ( NAME = nifi_log,
   	-- 自己sqlserver的DATA路径
      FILENAME = 'D:\databases\sqlserver\instance\MSSQL16.MSSQLSERVER\MSSQL\DATA\nifilog.ldf',
      SIZE = 5MB,
      MAXSIZE = 25MB,
      FILEGROWTH = 5MB ); 
  GO
  ```

  **注意：**

  需要对 `master` 数据库具有 CREATE DATABASE 权限，或者需要 CREATE ANY DATABASE 或 ALTER ANY DATABASE 权限。

  为了继续控制对 SQL Server 实例的磁盘使用，通常仅限几个 SQL Server 登录名具有“创建数据库”权限。

- 为某个数据库添加数据文件

  ```mysql
  USE master
  GO
  ALTER DATABASE nifi
  ADD FILEGROUP nifigroup;
  GO
  ALTER DATABASE nifi
  ADD FILE
  (
      NAME = nifidat1,
      FILENAME = 'D:\databases\sqlserver\instance\MSSQL16.MSSQLSERVER\MSSQL\DATA\nifidat1.mdf',
      SIZE = 5MB,
      MAXSIZE = 100MB,
      FILEGROWTH = 5MB
  ),
  (
      NAME = nifidat2,
      FILENAME = 'D:\databases\sqlserver\instance\MSSQL16.MSSQLSERVER\MSSQL\DATA\nifidat2.mdf',
      SIZE = 5MB,
      MAXSIZE = 100MB,
      FILEGROWTH = 5MB
  )
  TO FILEGROUP nifigroup;
  GO
  ```

- 删除数据库

  ```mysql
  USE master ;  
  GO  
  --多个数据库以逗号隔开
  DROP DATABASE nifi;  
  GO
  ```

  **删除前置操作：**

  1. 删除数据库中的所有数据库快照。
  2. 如果日志传送涉及数据库，请删除日志传送。
  3. 如果为事务复制发布了数据库，或将数据库发布或订阅到合并复制，请从数据库中删除复制。

  **注意：**

  1. 若要执行 DROP DATABASE 操作，用户必须至少对数据库具有 CONTROL 权限。
  2. 考虑对数据库进行完整备份。 只有通过还原备份才能重新创建已删除的数据库。
  3. 不能删除系统数据库。

- 创建表

  ```mysql
  use <database_name>;
  CREATE TABLE dbo.<table_name>(  
      column_name column_type(length) NOT NULL  
  ); 
  example:
  use nifi;
  create table dbo.multitype_column_table(
  	column_int int not null,
      column_smallint smallint not null,
      column_money money null,
      column_float float null,
      column_datetime datetime null
  );
  ```

- 添加注释

  ```mysql
  -- sqlserver用语句给表注释
  EXECUTE sp_addextendedproperty N'MS_Description', N'表注释', N'user', N'dbo', N'table', N'表名', NULL, NULL
  
  -- sqlserver用语句给表的“字段”注释
  EXECUTE sp_addextendedproperty N'MS_Description', N'字段注释', N'user', N'dbo', N'table', N'表名', N'column', N'字段名'
  
  -- 查看sqlserver注释
  SELECT
  A.name AS table_name,
  B.name AS column_name,
  C.value AS column_description
  FROM sys.tables A
  INNER JOIN sys.columns B ON B.object_id = A.object_id
  LEFT JOIN sys.extended_properties C ON C.major_id = B.object_id AND C.minor_id = B.column_id
  WHERE A.name = '表名' and B.name='列名'
  ```

- 删除表

  ```mysql
  DROP TABLE dbo.<table_name>;  
  ```

- 表重命名

  ```mysql
  EXEC sp_rename 'dbo.<旧表名>', '<新表名>';
  example:
  EXEC sp_rename 'dbo.multitype_column_table', 'new_multitype_column_table';
  ```

- 查看表定义

  ```mysql
  EXEC sp_help 'dbo.<表名>';
  example:
  EXEC sp_help 'dbo.nifi';
  ```

- 添加列

  ```mysql
  ALTER TABLE dbo.<table_name> ADD column_name1 VARCHAR(20) NULL, column_char char NULL;
  example:
  ALTER TABLE dbo.multitype_column_table ADD column_varchar VARCHAR(20) NULL, column_char char NULL;
  ```

- 修改列

  ```mysql
  ALTER TABLE <table_name> DROP COLUMN <column_name>;
  example:
  ALTER TABLE multitype_column_table DROP COLUMN column_char;
  ```

- 列的重命名

  ```mysql
  EXEC sp_rename '<table_name>.<旧列名>', '<新列名>', 'COLUMN';
  example:
  EXEC sp_rename 'multitype_column_table.column_varchar', 'column_varchar_new', 'COLUMN';
  ```

- 修改列

  ```mysql
  ALTER TABLE <table_name> ALTER COLUMN column_name DECIMAL(5, 2);
  example:
  alter table multitype_column_table alter column column_varchar VARCHAR(200);
  ```

- 指定列的默认值

  ```mysql
  CREATE TABLE dbo.doc_exz (
        column_a INT,
        -- 建表时指定列的默认值
        column_b INT DEFAULT 50
  );
  
  ALTER TABLE dbo.doc_exz ADD CONSTRAINT <列默认值的命名> DEFAULT 50 FOR column_b;
  example:
  ALTER TABLE dbo.multitype_column_table ADD CONSTRAINT df_multitype_column_table_column_float DEFAULT 50 FOR column_float;
  ```

- 在现有表中创建主键

  ```mysql
  ALTER TABLE Production.TransactionHistoryArchive ADD CONSTRAINT PK_TransactionHistoryArchive_TransactionID PRIMARY KEY CLUSTERED (TransactionID);
  example:
  ALTER TABLE dbo.multitype_column_table ADD CONSTRAINT pk_multitype_column_table_column_int PRIMARY KEY CLUSTERED (column_int);
  ```

- 在新表中创建主键

  ```mysql
  CREATE TABLE dbo.primary_key_table
     (
        id int IDENTITY (1,1) NOT NULL
        , CONSTRAINT pk_primary_key_table_id PRIMARY KEY CLUSTERED (id)
     )
  ;
  ```

- 在新表中创建聚集索引的主键

  ```mysql
  -- Create table to add the clustered index
  CREATE TABLE Production.TransactionHistoryArchive1
     (
        CustomerID uniqueidentifier DEFAULT NEWSEQUENTIALID()
        , TransactionID int IDENTITY (1,1) NOT NULL
        , CONSTRAINT PK_TransactionHistoryArchive1_CustomerID PRIMARY KEY NONCLUSTERED (CustomerID)
     )
  ;
  
  -- Now add the clustered index
  CREATE CLUSTERED INDEX CIX_TransactionID ON Production.TransactionHistoryArchive1 (TransactionID);
  ```

- 删除主键

  ```mysql
  USE AdventureWorks2012;  
  GO  
  -- Return the name of primary key.  
  SELECT name  
  FROM sys.key_constraints  
  WHERE type = 'PK' AND OBJECT_NAME(parent_object_id) = N'TransactionHistoryArchive';  
  GO  
  -- Delete the primary key constraint.  
  ALTER TABLE Production.TransactionHistoryArchive  
  DROP CONSTRAINT PK_TransactionHistoryArchive_TransactionID;   
  GO
  ```

- 创建函数

  ```mysql
  IF OBJECT_ID (N'dbo.sumId', N'FN') IS NOT NULL
      DROP FUNCTION dbo.sumId;
  GO
  CREATE FUNCTION dbo.sumId(@name varchar)
  RETURNS int
  AS
  -- Returns the stock level for the product.
  BEGIN
      DECLARE @ret int;
      SELECT @ret = SUM(p.id)
      FROM dbo.primary_key_table p
      WHERE p.name = @name;
       IF (@ret IS NULL)
          SET @ret = 0;
      RETURN @ret;
  END;
  
  select dbo.sumId('1asd');
  ```

- 执行

  ```mysql
  USE [AdventureWorks2016CTP3]
  GO
  
  -- Declare a variable to return the results of the function.
  DECLARE @ret nvarchar(15);
  
  -- Execute the function while passing a value to the @status parameter
  EXEC @ret = dbo.ufnGetSalesOrderStatusText @Status = 5;
  
  -- View the returned value.  The Execute and Select statements must be executed at the same time.
  SELECT N'Order Status: ' + @ret;
  
  -- Result:
  -- Order Status: Shipped
  example:
  USE [master]
  GO
  -- Declare a variable to return the results of the function.
  DECLARE @ret nvarchar(15);
  -- Execute the function while passing a value to the @status parameter
  EXEC @ret = dbo.sumId @name = '1asd';
  -- View the returned value.  The Execute and Select statements must be executed at the same time.
  SELECT N'sum id: ' + @ret;
  ```

