> 说明

1. 一般kafka和zookeeper是一起安装的，所以选择两个一起安装
2. kafka和zookeeper有版本对应关系，选择适当的版本避免兼容问题
3. kafka依赖zookeeper，因此需要先安装并启动zookeeper
4. 选择kafka_2.12-2.7.2和zookeeper-3.5.9进行安装

> kafka与zookeeper版本对应关系

| Kafka版本           | Zookeeper版本    |
| ------------------- | ---------------- |
| kafka_2.12-2.7.2    | zookeeper-3.5.9  |
| …                   | …                |
| kafka_2.12-2.4.0    | zookeeper-3.5.6  |
| kafka_2.12-2.3.1    | zookeeper-3.4.14 |
| kafka_2.12-2.3.0    | zookeeper-3.4.14 |
| kafka_2.12-1.1.1    | zookeeper-3.4.10 |
| kafka_2.12-1.1.0    | zookeeper-3.4.10 |
| kafka_2.12-1.0.2    | zookeeper-3.4.10 |
| kafka_1.0.0         | zookeeper-3.4.10 |
| kafka_2.12-0.11.0.0 | zookeeper-3.4.10 |
| kafka_2.12-0.10.2.2 | zookeeper-3.4.9  |
| kafka_2.11-0.10.0.0 | zookeeper-3.4.6  |
| kafka_2.11-0.9.0.0  | zookeeper-3.4.6  |

> kafka与zookeeper历史版本下载地址

1. [Index of /dist/kafka (apache.org)](http://archive.apache.org/dist/kafka/)
2. [Index of /dist/zookeeper (apache.org)](http://archive.apache.org/dist/zookeeper/)

> zookeeper安装

- 下载zookeeper-3.5.9

  http://archive.apache.org/dist/zookeeper/zookeeper-3.5.9/apache-zookeeper-3.5.9-bin.tar.gz

- 安装zookeeper-3.5.9

  ```shell
  # 将下载的 apache-zookeeper-3.5.9-bin.tar.gz 安装到指定路径下，比如这里是：D:\environments\zookeeper
  # 进入 D:\environments\zookeeper\apache-zookeeper-3.5.9-bin 目录，创建 data 和 logs 两个文件夹
  # 进入 D:\environments\zookeeper\apache-zookeeper-3.5.9-bin\conf 目录，拷贝 zoo_sample.cfg 为 zoo.cfg
  # 修改 zoo.cfg 文件
  dataDir=D:\environments\zookeeper\apache-zookeeper-3.5.9-bin\data
  dataLogDir=D:\environments\zookeeper\apache-zookeeper-3.5.9-bin\logs
  ```

- 启动zookeeper

  ```shell
  # 打开cmd，进入zookeeper的bin目录
  cd D:\environments\zookeeper\apache-zookeeper-3.5.9-bin\bin
  # 启动 zkServer
  zkServer.cmd
  ```

- 连接zookeeper

  ```shell
  # 打开cmd，进入zookeeper的bin目录
  cd D:\environments\zookeeper\apache-zookeeper-3.5.9-bin\bin
  # 启动 zkCli
  zkCli.cmd
  # 连接成功显示
  ------
  WatchedEvent state:SyncConnected type:None path:null
  [zk: localhost:2181(CONNECTED) 0]
  ------
  ```

> kafka安装

- 下载kafka_2.12-2.7.2

  http://archive.apache.org/dist/kafka/2.7.2/kafka_2.13-2.7.2.tgz

- 安装kafka_2.13-2.7.2

  ```shell
  # 将下载的kafka_2.13-2.7.2.tgz安装到指定路径下，比如这里是：D:\environments\kafka
  # 创建kafka日志保存路径：进入D:\environments\kafka\kafka_2.13-2.7.2目录，创建 kafka-logs 文件夹	
  # 进入D:\environments\kafka\kafka_2.13-2.7.2\config，修改 server.properties 文件
  log.dirs=D:\environments\kafka\kafka_2.13-2.7.2\kafka-logs
  ```

- 启动 kafka-server

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-server-start.bat ../../config/server.properties
  # 控制台最后显示日志，则启动成功
  ------
  [KafkaServer id=0] started (kafka.server.KafkaServer)
  ------
  ```

- 创建kafka主题

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic hello-kafka
  # 创建成功显示
  ------
  Created topic hello-kafka.
  ------
  ```

- 查看所有主题

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-topics.bat --zookeeper localhost:2181 --list
  # 展示出所有主题
  ------
  hello-kafka
  ------
  ```

- 查看主题信息

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-topics.bat --zookeeper localhost:2181 --describe --topic hello-kafka
  # 展示topic信息
  ------
  Topic: hello-kafka      PartitionCount: 1       ReplicationFactor: 1    Configs:
          Topic: hello-kafka      Partition: 0    Leader: 0       Replicas: 0     Isr: 0
  ------
  ```

- 生产者创建消息

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-console-producer.bat --broker-list localhost:9092 --topic hello-kafka
  # 不要关闭控制台，输入消息按回车
  {“message”:"hello kafka!"}
  ------
  D:\environments\kafka\kafka_2.13-2.7.2\bin\windows>kafka-console-producer.bat --broker-list localhost:9092 --topic hello-kafka
  >{"message":"hello kafka!"}
  ------
  ```

- 消费者消费消息

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic hello-kafka --from-beginning
  # 不要关闭控制台，观察生产者创建消息后，能不能收到消息
  ------
  D:\environments\kafka\kafka_2.13-2.7.2\bin\windows>kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic hello-kafka --from-beginning
  {"message":"hello kafka!"}
  ------
  ```

- 删除主题

  ```shell
  # 打开cmd，进入 D:\environments\kafka\kafka_2.13-2.7.2\bin\windows目录
  cd D:\environments\kafka\kafka_2.13-2.7.2\bin\windows
  kafka-topics.bat --zookeeper localhost:2181 --delete --topic hello-kafka
  # 删除成功显示
  ------
  D:\environments\kafka\kafka_2.13-2.7.2\bin\windows>kafka-topics.bat --zookeeper localhost:2181 --delete --topic hello-kafka
  Topic hello-kafka is marked for deletion.
  Note: This will have no impact if delete.topic.enable is not set to true.
  ------
  # 再次查看主题列表，可以看到刚刚删除的主题已经被标记为删除
  ------
  D:\environments\kafka\kafka_2.13-2.7.2\bin\windows>kafka-topics.bat --zookeeper localhost:2181 --list
  __consumer_offsets
  hello-kafka - marked for deletion
  ------
  ```

  ```json
  {
      "name":"kafka-message",
      "amount":${random():mod(10):plus(1)},
      "create_date":"${now():format('yyyy-MM-dd')}",
      "create_time":${now():toNumber()},
      "content":"${UUID()}"
  }
  
  {
      "name":"kafka-message",
      "amount":5,
      "create_date":"2023-01-16",
      "create_time":1673803487120,
      "content":"b7bec68a-72fd-4608-bde0-438e56bc8b62"
  }
  ```
  
  