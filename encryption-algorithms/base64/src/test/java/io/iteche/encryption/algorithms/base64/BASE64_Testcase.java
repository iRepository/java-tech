package io.iteche.encryption.algorithms.base64;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.util.UUID;

/**
 * @author hegh1
 * @desc
 * Base64 编码是我们程序开发中经常使用到的编码方法，它用 64 个可打印字符来表示二进制数据。这 64 个字符是：小写字母 a-z、大写字母 A-Z、数字 0-9、符号"+“、”/“（再加上作为垫字的”="，实际上是 65 个字符），其他所有符号都转换成这个字符集中的字符。Base64 编码通常用作存储、传输一些二进制数据编码方法，所以说它本质上是一种将二进制数据转成文本数据的方案。
 * 通常用作对二进制数据进行加密
 * @date 2023/2/2 14:53
 */
public class BASE64_Testcase {

    static final BASE64Decoder base64Decoder = new BASE64Decoder();
    static final BASE64Encoder BASE_64_ENCODER = new BASE64Encoder();

    public static void main(String[] args) throws IOException {
        String content = UUID.randomUUID().toString();
        System.out.println(content);
        byte[] bytes = base64Decoder.decodeBuffer(content);
        System.out.println(BASE_64_ENCODER.encode(bytes));
    }

}
