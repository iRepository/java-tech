package io.iteche.encryption.algorithms.bcrypt;

import at.favre.lib.crypto.bcrypt.BCrypt;

import java.security.SecureRandom;
import java.util.Base64;

/**
 * @author hegh1
 * @desc
 * @date 2023/2/2 14:43
 */
public class BCrypt_Testcase {

    public static void main(String[] args) {
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        String password = passwordGenerator.generate();
        System.out.println(password);
        BCryptEncoder bCryptEncoder = new BCryptEncoder();
        System.out.println(bCryptEncoder.encode(password.toCharArray()).length());
        System.out.println(bCryptEncoder.encode(password.toCharArray()).length());
        System.out.println(bCryptEncoder.encode(password.toCharArray()));
        System.out.println(bCryptEncoder.encode(password.toCharArray()));
    }

    static class BCryptEncoder {
        private static final int DEFAULT_COST = 12;

        private static final BCrypt.Version BCRYPT_VERSION = BCrypt.Version.VERSION_2B;

        private static final BCrypt.Hasher HASHER = BCrypt.with(BCRYPT_VERSION);

        private static final BCrypt.Verifyer VERIFYER = BCrypt.verifyer(BCRYPT_VERSION);

        /**
         * Encode Password and return bcrypt hashed password
         *
         * @param password Password
         * @return bcrypt hashed password
         */
        public String encode(final char[] password) {
            return HASHER.hashToString(DEFAULT_COST, password);
        }

        /**
         * Match Password against bcrypt hashed password
         *
         * @param password Password to be matched
         * @param encodedPassword bcrypt hashed password
         * @return Matched status
         */
        public boolean matches(final char[] password, final String encodedPassword) {
            final BCrypt.Result result = VERIFYER.verifyStrict(password, encodedPassword.toCharArray());
            return result.verified;
        }
    }

    static class PasswordGenerator {

        private static final Base64.Encoder RANDOM_BYTE_ENCODER = Base64.getEncoder().withoutPadding();
        private static final int RANDOM_BYTE_LENGTH = 24;

        public String generate() {
            final SecureRandom secureRandom = new SecureRandom();
            final byte[] bytes = new byte[RANDOM_BYTE_LENGTH];
            secureRandom.nextBytes(bytes);
            return RANDOM_BYTE_ENCODER.encodeToString(bytes);
        }

    }

}
