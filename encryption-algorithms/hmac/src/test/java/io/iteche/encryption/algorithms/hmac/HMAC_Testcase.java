package io.iteche.encryption.algorithms.hmac;

import sun.misc.BASE64Encoder;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.UUID;

/**
 * @author hegh1
 * @desc
 * 支持的algorithm: HmacMD5，HmacSHA1 ，HmacSHA256，HmacSHA512等等
 * 对称密钥key：预先商量的密钥。HMAC算法的安全强度是由对称密钥的安全强度和散列函数的安全强度*2 之间的最小值决定的。
 * 数据data: 待验证的消息
 *
 * HMAC(Hash Message Authentication Code ， 散列消息鉴别码)
 * 使用一个密钥生成一个固定大小的小数据块，即MAC，并将其加入到消息中，然后传输。接收方利用与发送方共享的密钥进行鉴别认证
 * @date 2023/2/2 15:40
 */
public class HMAC_Testcase {

    public static final String KEY_MAC = "HmacMD5";
    static final BASE64Encoder BASE_64_ENCODER = new BASE64Encoder();
    static final String DEFAULT_KEY = "!@#123KK#$%";

    /***
     * 初始化HMAC密钥
     * @return
     * @throws Exception
     */
    public static String initMacKey() throws Exception{
        KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_MAC);
        SecretKey secretKey = keyGenerator.generateKey();
        return BASE_64_ENCODER.encode(secretKey.getEncoded());
    }

    /**
     * HMAC加密
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryHMAC(byte[] data, String key) throws Exception{
        SecretKey secreKey = new SecretKeySpec(key.getBytes(), KEY_MAC);
        Mac mac = Mac.getInstance(secreKey.getAlgorithm());
        mac.init(secreKey);
        return mac.doFinal();
    }

    /**
     * HMAC加密
     * @param data
     * @return
     * @throws Exception
     */
    public static byte[] encryHMAC(byte[] data) throws Exception{
        return encryHMAC(data, DEFAULT_KEY);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(initMacKey());
        String password = UUID.randomUUID().toString();
        byte[] bytes = encryHMAC(password.getBytes());
    }

}
