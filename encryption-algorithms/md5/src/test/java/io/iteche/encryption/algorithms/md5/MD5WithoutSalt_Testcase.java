package io.iteche.encryption.algorithms.md5;

import java.security.MessageDigest;
import java.util.UUID;

/**
 * @author hegh1
 * @desc
 * MD5 是将任意长度的数据字符串转化成短小的固定长度的值的单向操作，任意两个字符串不应有相同的散列值。因此 MD5 经常用于校验字符串或者文件，因为如果文件的 MD5 不一样，说明文件内容也是不一样的，如果发现下载的文件和给定的 MD5 值不一样，就要慎重使用。
 * MD5 主要用做数据一致性验证、数字签名和安全访问认证，而不是用作加密。比如说用户在某个网站注册账户时，输入的密码一般经过 MD5 编码，更安全的做法还会加一层盐（salt），这样密码就具有不可逆性。然后把编码后的密码存入数据库，下次登录的时候把密码 MD5 编码，然后和数据库中的作对比，这样就提升了用户账户的安全性。
 * 是一种单向加密算法，只能加密不能解密
 * @date 2023/2/2 15:03
 */
public class MD5WithoutSalt_Testcase {

    /**
     * 使用MD5对密码进行加密
     * 先取密码的首尾两个字符进行md5生成临时字符，然后将原始密码与临时字符拼接再次进行md5
     *
     * @param password 明文密码
     * @return 加密后的密码
     */
    public String encode(String password) {
        String tmpStr = password.substring(0, 1) + password.substring(password.length() - 1);
        String tmpMd5 = md5(tmpStr);
        String content = password + tmpMd5;
        return md5(content);
    }

    public String md5(String source) {
        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            byte[] strTemp = source.getBytes();
            // 使用MD5创建MessageDigest对象
            MessageDigest mdTemp = MessageDigest.getInstance("MD5");
            mdTemp.update(strTemp);
            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] str = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte b = md[i];
                str[k++] = hexDigits[b >> 4 & 0xf];
                str[k++] = hexDigits[b & 0xf];
            }

            return new String(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        MD5WithoutSalt_Testcase md5WithoutSalt_testcase = new MD5WithoutSalt_Testcase();
        String password = UUID.randomUUID().toString();
        System.out.println(password);
        String encode = md5WithoutSalt_testcase.encode(password);
        System.out.println(encode);
        System.out.println(md5WithoutSalt_testcase.md5(password));
    }

}
