create user 'emp'@'localhost' identified by 'emp@txyEcs121';
create database emp;
grant all on *.* to 'emp'@'localhost';
flush privileges;

drop table emp_express_order;


create table emp_express_order
(
    id                  bigint auto_increment comment 'id'
        primary key,
    denpyo_number       varchar(200) charset utf8 not null comment '商品名',
    kanri_type          varchar(50) charset utf8  not null comment '订单号',
    kanri_number        varchar(100) charset utf8 not null comment '商品简称',
    user_name           varchar(16) charset utf8  null comment '退款成功状态',
    kanri_name          varchar(200) charset utf8 not null comment '快递公司名称',
    building_name       varchar(50) charset utf8  not null comment '快递单号',
    location            timestamp                   null comment '发货时间',
    detail_location     decimal(20, 2)              not null comment '金额',
    remarks             int                         not null comment '数量',
    kanri_status        varchar(100) charset utf8 not null comment '备注1',
    tyosa_result        varchar(100) charset utf8 not null comment '备注2',
    tyosa_date          varchar(100) charset utf8 not null comment '备注3',
    tyosa_did_name      varchar(100) charset utf8 not null comment '备注4',
    tyosa_did_name_code varchar(100) charset utf8 not null comment '备注5',
    tanent_id           int default -1              not null,
    constraint uk_order_no
        unique (kanri_type, tanent_id)
)
    comment '快递订单表' charset = utf8;