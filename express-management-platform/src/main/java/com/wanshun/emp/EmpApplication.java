package com.wanshun.emp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 21:29
 */
@SpringBootApplication
@EnableSwagger2
@MapperScan({"com.wanshun.emp.mapper"})
public class EmpApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmpApplication.class, args);
    }

}
