package com.wanshun.emp.controller;

import com.wanshun.emp.exception.ExceptionResponseHandler;
import com.wanshun.emp.model.dto.ApiResponse;
import com.wanshun.emp.model.dto.BulkUploadDto;
import com.wanshun.emp.model.dto.Model;
import com.wanshun.emp.service.IExpressOrderService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 21:35
 */
@RequestMapping("/api/v1/express/order")
@RestController
@Api("快递单号")
public class ExpressOrderController extends ExceptionResponseHandler {

    @Resource
    IExpressOrderService iExpressOrderService;

    @PostMapping("/bulk-upload")
    public ApiResponse<Boolean> bulkUpload(@RequestBody BulkUploadDto bulkUploadDto) {
        return ApiResponse.buildSuccess(iExpressOrderService.bulkUpload(bulkUploadDto.getOrderList()));
    }

    @GetMapping("/page-query")
    public ApiResponse<List<Model>> pageQuery() {

    }

}
