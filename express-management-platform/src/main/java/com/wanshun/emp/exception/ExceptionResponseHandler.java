package com.wanshun.emp.exception;

import com.wanshun.emp.model.dto.ApiResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/23 22:20
 */
public abstract class ExceptionResponseHandler {

    public ExceptionResponseHandler() {
    }

    @ExceptionHandler(Exception.class)
    public ApiResponse genericExceptionHandler(Exception exception) {
        return ApiResponse.buildFailure(500, exception.getMessage());
    }

}
