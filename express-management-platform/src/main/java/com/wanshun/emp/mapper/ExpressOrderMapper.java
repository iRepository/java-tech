package com.wanshun.emp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wanshun.emp.model.po.ModelPo;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/22 11:23
 */
public interface ExpressOrderMapper extends BaseMapper<ModelPo> {
}
