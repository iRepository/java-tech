package com.wanshun.emp.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 21:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApiResponse<T> {

    private int code;
    private String message;
    private T data;

    public static <T> ApiResponse<T> create(int code, String message, T data) {
        return new ApiResponse(code, message, data);
    }

    public static <T> ApiResponse<T> buildSuccess(T data) {
        ApiResponse<T> apiResponse = new ApiResponse();
        apiResponse.setCode(200);
        apiResponse.setMessage("成功");
        apiResponse.setData(data);
        return apiResponse;
    }

    public static ApiResponse buildFailure(int code, String message) {
        return ApiResponse.create(code, message, false);
    }

}
