package com.wanshun.emp.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 21:45
 */
@Data
public class BulkUploadDto implements Serializable {

    @ApiModelProperty(value = "订单号数组", required = true)
    private List<Model> orderList;

}
