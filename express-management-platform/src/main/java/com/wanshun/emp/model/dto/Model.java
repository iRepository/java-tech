package com.wanshun.emp.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 21:33
 */
@Data
public class Model implements Serializable {

    @ApiModelProperty(value = "商品名", required = true)
    private String goodsName;

    @ApiModelProperty(value = "订单号", required = true)
    private String orderNo;

    @ApiModelProperty("商品简称")
    private String goodsShortName;

    @ApiModelProperty("退款成功状态")
    private String refundStatus;

    @ApiModelProperty(value = "快递公司名称", required = true)
    private String expressCompanyName;

    @ApiModelProperty(value = "快递单号", required = true)
    private String expressNo;

    @ApiModelProperty("发货时间：yyyy-MM-dd HH:mm:ss")
    private String deliveryTime;

    @ApiModelProperty(value = "金额", required = true)
    private String amount;

    @ApiModelProperty(value = "数量", required = true)
    private String count;

    @ApiModelProperty("备注1")
    private String remark1;

    @ApiModelProperty("备注2")
    private String remark2;

    @ApiModelProperty("备注3")
    private String remark3;

    @ApiModelProperty("备注4")
    private String remark4;

    @ApiModelProperty("备注5")
    private String remark5;

}
