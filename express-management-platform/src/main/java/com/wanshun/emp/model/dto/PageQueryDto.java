package com.wanshun.emp.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/23 22:33
 */
@Data
public class PageQueryDto implements Serializable {

    @ApiModelProperty(value = "搜索内容")
    private String searchkey;

    @ApiModelProperty(value = "发货时间")
    private String location;

    @ApiModelProperty(value = "状态 0：签收，1： 待签收，2：待上传")
    private String status;

    @ApiModelProperty(value = "请求数量", required = true)
    private Integer size;

    @ApiModelProperty(value = "当前页", required = true)
    private Integer page;

}
