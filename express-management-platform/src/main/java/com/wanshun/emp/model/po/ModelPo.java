package com.wanshun.emp.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wanshun.emp.model.dto.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 22:24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("emp_express_order")
public class ModelPo implements Serializable {

    private Long id;

    @TableField("denpyo_number")
    private String goodsName;

    @TableField("kanri_type")
    private String orderNo;

    @TableField("kanri_number")
    private String goodsShortName;

    @TableField("user_name")
    private String refundStatus;

    @TableField("kanri_name")
    private String expressCompanyName;

    @TableField("building_name")
    private String expressNo;

    @TableField("location")
    private LocalDateTime deliveryTime; 

    @TableField("detail_location")
    private BigDecimal amount;

    @TableField("remarks")
    private int count;

    @TableField("kanri_status")
    private String remark1;

    @TableField("tyosa_result")
    private String remark2;

    @TableField("tyosa_date")
    private String remark3;

    @TableField("tyosa_did_name")
    private String remark4;

    @TableField("tyosa_did_name_code")
    private String remark5;

    public static ModelPo from(Model source) {
        ModelPo modelPo = new ModelPo();
        BeanUtils.copyProperties(source, modelPo);
        modelPo.setAmount(new BigDecimal(source.getAmount()));
        modelPo.setCount(Integer.parseInt(source.getCount()));
        modelPo.setDeliveryTime(LocalDateTime.parse(source.getDeliveryTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return modelPo;
    }

}
