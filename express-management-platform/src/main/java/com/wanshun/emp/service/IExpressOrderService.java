package com.wanshun.emp.service;

import com.wanshun.emp.model.dto.Model;

import java.util.List;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/21 21:48
 */
public interface IExpressOrderService {

    boolean bulkUpload(List<Model> orderList);

    List<Model> exists(List<Model> orderList);

}
