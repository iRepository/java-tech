package com.wanshun.emp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wanshun.emp.mapper.ExpressOrderMapper;
import com.wanshun.emp.model.dto.Model;
import com.wanshun.emp.model.po.ModelPo;
import com.wanshun.emp.service.IExpressOrderService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/22 11:21
 */
@Service
public class ExpressOrderServiceImpl extends ServiceImpl<ExpressOrderMapper, ModelPo> implements IExpressOrderService {

    @Override
    public boolean bulkUpload(List<Model> orderList) {
        List<Model> modelList = this.exists(orderList);
        List<ModelPo> modelPoList = modelList.stream().map(ModelPo::from).collect(Collectors.toList());
        return this.saveBatch(modelPoList);
    }

    @Override
    public List<Model> exists(List<Model> orderList) {
        LambdaQueryWrapper<ModelPo> wrapper = Wrappers.lambdaQuery(ModelPo.class);
        List<String> orderNoList = orderList.stream().map(Model::getOrderNo).collect(Collectors.toList());
        wrapper.in(ModelPo::getOrderNo, orderNoList);
        List<ModelPo> modelPoList = getBaseMapper().selectList(wrapper);
        List<String> collect = modelPoList.stream().map(ModelPo::getOrderNo).collect(Collectors.toList());
        return orderList.stream().filter(o -> !collect.contains(o)).collect(Collectors.toList());
    }

}
