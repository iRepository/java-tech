package com.wanshun.emp.utils;

import com.github.f4b6a3.ulid.UlidCreator;

/**
 * @author hegh1
 * @desc
 * @date 2023/1/23 21:10
 */
public class Generator {

    public static void main(String[] args) {
        // 常规
        System.out.println(UlidCreator.getUlid());
        // 单调排序
        System.out.println(UlidCreator.getMonotonicUlid());
    }

}
